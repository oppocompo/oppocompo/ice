# ICE
Interactive Control Environment (ICE)  is a graphical editor that support the edition of emerging ambient applications. 

In order to install it, you should follow these steps:

  1. Download GEMOC Studio from http://gemoc.org/studio.html and install it.

  2. Download `ice-plugin.zip` and `ice-workspace.zip`

  3. Unzip `ice-plugin.zip` and put the content of its `plugins` directory into the `plugins` directory of GEMOC

  4. Start GEMOC, go to `File -> import -> general -> projects from folder or archive -> Archive... -> select -ce-workspace.zip -> finish`

  5. Edit `org.eclipse.iceeditor.design/src/filepaths/Filepaths.java` :

    - For ICE_INPUT_FILE_PATH : absolute (recommended) path to `org.eclipse.iceEditor.sample/editor.ice_editor`
    - The path to this file must also be copied into `oce/src/resources/config.properties` as `ice.input_filepath`
    - For ICE_OUTPUT_FILE_PATH : absolute path to `oce/src/main/resources/ice_output/assembly.ice_editor`

  6. Open the representation by double clicking on `org.eclipse.iceEditor.sample/representations.aird -> environment -> Environment -> ICE Editor`

You can now edit the assembly's bindings, and accept/reject the proposition with the right click menu
